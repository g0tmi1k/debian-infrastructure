from __future__ import (absolute_import, division, print_function)
__metaclass__ = type


def arch_is_allowed(item, arch):
    allowed_arches = item.get('allowed_architectures', [arch])
    return arch in allowed_arches


def sbuild_configure(distlist, archlist):
    ret = []

    for item in distlist:
        if isinstance(item, dict):
            # We have detailed configuration requested
            if 'architecture' in item:
                # If arch is specified, keep as-is
                ret.append(item.copy())
            else:
                # If arch is not specified, then we create
                # duplicates for each requested & allowed architecture
                for arch in archlist:
                    if not arch_is_allowed(item, arch):
                        continue
                    entry = item.copy()
                    entry['architecture'] = arch
                    ret.append(entry)
        else:
            # We only have the name of the distribution
            for arch in archlist:
                ret.append({
                    'distribution': item,
                    'architecture': arch,
                })

    for entry in ret:
        if 'extra_aliases' not in entry:
            continue
        entry['extra_aliases'] = [
            item.format(arch=entry['architecture'])
            for item in entry['extra_aliases']
        ]

    return ret


class FilterModule(object):
    ''' Sbuild filters '''

    def filters(self):
        return {
            'sbuild_configure': sbuild_configure,
        }
