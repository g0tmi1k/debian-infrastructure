Reprepro
========

Configure a package repository with reprepro. The role can also setup an
incoming directory used to upload new packages in the repository.

The roles provides a `$reprepro_basedir/bin/trigger-build` script that can
be used in a reprepro hook to trigger the build process of binary packages
when a new source package is accepted. This script assumes that
`$reprepro_basedir/bin/schedule-build $PACKAGE $VERSION $DISTRIBUTION
$ARCHITECTURES` will schedule the requested build on the requested
architectures. To setup the hook you need a `Log` field in reprepro's
distribution file:

```
Log: something.log
  --type=dsc --via=processincoming /srv/.../bin/trigger-build
```

Role Variables
--------------

Mandatory variables:
* `reprepro_basedir`: top-level directory where all files and directories
  will be placed
* `reprepro_user`: user owning all the files
* `reprepro_templates`: dictionnary mapping a reprepro configuration file
  (distributions, updates, pulls, uploaders, incoming) or some other important file
  (`authorized_keys`) to the name of a template file used to initialize
  said file.
* `reprepro_gpg_key_realname` and `reprepro_gpg_key_email`: name and email
  used to create the GPG key that will be used to sign the repository
  (mandatory unless `reprepro_enable_signature` has been set to false)

Optional variables:
* `reprepro_upload_user`: dedicated user that will have be used to upload
  files via SSH to the incoming queue (defaults to the value of the
  `reprepro_user` variable)
* `reprepro_incomingdir`: path of the incoming directory (defaults to
  the "incoming" directory below `reprepro_basedir`)
* `reprepro_enable_incoming`: enable or disable the incoming directory
  (defaults to "false")
* `reprepro_incoming_rule` (mandatory if `reprepro_enable_incoming` is
  true): the name of the incoming rule to apply
* `reprepro_admin_email` (mandatory if `reprepro_enable_incoming` is
  true): email of the repository admin, used as From in a failed upload
  notification email.
* `reprepro_dir`: directory where reprepro files will be setup (defaults
  to the "reprepro" directory below `reprepro_basedir`)
* `reprepro_artifactdir`: directory where the GPG armored key used to
  validate the repository will be made available (defaults to
  `reprepro_dir`), can be disabled by setting a false-ish value
* `reprepro_tmpdir`: temporary directory (defaults to the "tmp" directory
  below `reprepro_basedir`)
* `reprepro_logdir`: directory hosting various log files (defaults to the
  "logs" directory below `reprepro_basedir`)
* `reprepro_morguedir`: directory hosting a copy of all newly uploaded
  files (defaults to the "morgue" directory below `reprepro_basedir`)
* `reprepro_changesdir`: directory for archiving successfully processed
  .changes files (defaults to unset)
* `reprepro_repositorydir`: when non-empty, it defines the location for
  the "dists" and "pool" directories that are otherwise stored in
  `reprepro_dir`
* `reprepro_trigger_build_rules`: shell snippet in a case statement that
  helps to map the "codename" seen by reprepro to a "distribution" name
  (stored in `$dist`) used to build the package.
* `reprepro_enable_signature`: set to false to not generate any GPG key
  (defaults to true)

Example Playbook
----------------

```
- name: Setup reprepro repositories for deblts
  ansible.builtin.include_role:
    name: freexian.debian_infrastructure.reprepro
    apply:
      tags:
        - reprepro
  vars:
    reprepro_user: deblts
    reprepro_upload_user: extended-lts
    reprepro_admin_email: sysadmin@freexian.com
    reprepro_basedir: /srv/deb.freexian.com
    reprepro_gpg_key_realname: Extended LTS Repository
    reprepro_gpg_key_email: sysadmin@freexian.com
    reprepro_enable_incoming: true
    reprepro_incoming_rule: elts
    reprepro_trigger_build_rules: |
      # Custom rules for LTS/ELTS
          *-lts)
        dist=${codename%%-lts}
        ;;
    reprepro_templates:
      distributions: elts-reprepro-distributions.j2
      updates: elts-reprepro-updates.j2
      pulls: elts-reprepro-pulls.j2
      uploaders: elts-reprepro-uploaders.j2
      incoming: elts-reprepro-incoming.j2
      authorized_keys: elts-authorized-keys.j2
```

License
-------

MIT

Author Information
------------------

Raphaël Hertzog <raphael@freexian.com>
