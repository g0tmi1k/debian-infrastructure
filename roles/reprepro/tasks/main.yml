---
- name: Gather required facts
  ansible.builtin.setup:
    gather_subset:
      - distribution
      - distribution_major_version

- name: Set up bullseye-backports to enable zstd support in reprepro
  block:
    - name: Copy bullseye-backports.sources
      ansible.builtin.copy:
        content: |
          deb http://deb.debian.org/debian bullseye-backports main
        dest: /etc/apt/sources.list.d/bullseye-backports.list
      register: bullseye_backports_sources

    - name: Update apt cache
      ansible.builtin.apt:
        update_cache: yes
      when: bullseye_backports_sources.changed

    - name: Set target release for reprepro
      ansible.builtin.set_fact:
        reprepro_default_release: bullseye-backports

  when: ansible_facts['distribution'] == "Debian" and ansible_facts['distribution_major_version'] | int == 11

- name: Install reprepro package
  ansible.builtin.apt:
    pkg: reprepro
    default_release: "{{ reprepro_default_release|default('') }}"

- name: Install required packages
  ansible.builtin.apt:
    pkg:
      - gnupg

- name: Create default user
  ansible.builtin.user:
    name: '{{ reprepro_user }}'
    create_home: yes
  register: user_data

- name: Create all reprepro directories
  ansible.builtin.file:
    path: '{{ reprepro_dir }}/{{ item }}'
    state: directory
    mode: 0755
    owner: '{{ reprepro_user }}'
    group: '{{ reprepro_user }}'
  loop:
    - conf
    - db
    - lists
    - logs
    - bin

- name: Create repository related directories
  ansible.builtin.file:
    path: '{{ reprepro_repositorydir }}/{{ item }}'
    state: directory
    mode: 0755
    owner: '{{ reprepro_user }}'
    group: '{{ reprepro_user }}'
  loop:
    - dists
    - pool

- name: Create symlinks for repository related directories
  ansible.builtin.file:
    path: '{{ reprepro_dir }}/{{ item }}'
    state: link
    src: '{{ reprepro_repositorydir }}/{{ item }}'
    owner: '{{ reprepro_user }}'
    group: '{{ reprepro_user }}'
  loop:
    - dists
    - pool
  when: reprepro_repositorydir != reprepro_dir

- name: Create service directories with group write
  ansible.builtin.file:
    path: '{{ lookup("vars", item) }}'
    state: directory
    mode: 0775
    owner: '{{ reprepro_user }}'
    group: '{{ reprepro_user }}'
  loop:
    - reprepro_tmpdir
    - reprepro_logdir
    - reprepro_morguedir

- name: Create .changes archive directory
  ansible.builtin.file:
    path: '{{ reprepro_changesdir }}'
    state: directory
    mode: '0755'
    owner: '{{ reprepro_user }}'
    group: '{{ reprepro_user }}'
  when: 'reprepro_changesdir is defined'

- name: Create artifact directory
  ansible.builtin.file:
    path: '{{ reprepro_artifactdir }}'
    state: directory
    mode: 0755
    owner: '{{ reprepro_user }}'
    group: '{{ reprepro_user }}'
  when: reprepro_artifactdir and reprepro_artifactdir != reprepro_dir

- name: Set up GPG signing key
  block:
    - name: Create GPG key
      ansible.builtin.include_role:
        name: freexian.debian_infrastructure.gpg
        tasks_from: genkey
      vars:
        gpg_user: '{{ reprepro_user }}'
        gpg_key_realname: '{{ reprepro_gpg_key_realname }}'
        gpg_key_email: '{{ reprepro_gpg_key_email }}'
        gpg_key_type: RSA
        gpg_key_length: 4096
        gpg_key_usage: sign
        gpg_expire_date: '5y'

    - name: Export archive key (binary keyring)
      ansible.builtin.shell:
        cmd: 'gpg --export-options export-minimal --export {{ reprepro_gpg_key_email }} > {{ reprepro_artifactdir }}/archive-key.gpg'
        creates: '{{ reprepro_artifactdir }}/archive-key.gpg'
      become: yes
      become_user: '{{ reprepro_user }}'
      when: reprepro_artifactdir

    - name: Export archive key (ascii armored)
      ansible.builtin.shell:
        cmd: 'gpg --armor --export-options export-minimal --export {{ reprepro_gpg_key_email }} > {{ reprepro_artifactdir }}/archive-key.asc'
        creates: '{{ reprepro_artifactdir }}/archive-key.asc'
      become: yes
      become_user: '{{ reprepro_user }}'
      when: reprepro_artifactdir

    - name: Extract gpg key id
      ansible.builtin.shell:
        cmd: 'gpg --with-colon --list-keys {{ reprepro_gpg_key_email }} | grep ^pub: | head -n 1 | cut -d: -f 5'
      become: yes
      become_user: '{{ reprepro_user }}'
      register: gpg_list_keys_result
      check_mode: false
      changed_when: false

  when: reprepro_enable_signature

- name: Define variable with gpg key id
  ansible.builtin.set_fact:
    reprepro_gpg_key_id: '{{ gpg_list_keys_result.stdout | default("no-key-available") }}'
  when: reprepro_gpg_key_id is undefined

- name: Create configuration files from templates
  ansible.builtin.template:
    src: '{{ reprepro_templates[item] }}'
    dest: '{{ reprepro_dir }}/conf/{{ item }}'
    owner: '{{ reprepro_user }}'
  loop:
    - distributions
    - updates
    - pulls
    - uploaders
    - incoming
    - override
  when: 'item in reprepro_templates'
  register: reprepro_configuration_updates
  tags:
    - buildd_data

# Updates (or creates if required) the repository metadata files
# (Packages, Sources, Release, etc.)
- name: Run reprepro export
  ansible.builtin.command:
    cmd: reprepro export
    chdir: '{{ reprepro_dir }}'
  become: yes
  become_user: '{{ reprepro_user }}'
  when: reprepro_configuration_updates.changed

- name: Place templatized repository tools
  ansible.builtin.template:
    src: 'reprepro-{{ item }}.j2'
    dest: '{{ reprepro_dir }}/bin/{{ item }}'
    mode: 0755
    owner: '{{ reprepro_user }}'
  loop:
    - trigger-build

- name: Setup incoming directory
  ansible.builtin.include_tasks:
    file: incoming.yml
    apply:
      tags:
        - incoming
  when: reprepro_enable_incoming
