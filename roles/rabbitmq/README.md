rabbitmq
========

The role will configure a dropin directory (by default /etc/rabbitmq/conf.d)
such that individual aspects can be configured in individual files.

The clustering interface is bound to loopback to avoid exposing it on the
internet. This effectively disables clustering. The same goes for epmd's
socket.

License
-------

MIT

Author Information
------------------
Sébastien Delafond <sdelafond@gmail.com>
Helmut Grohne <helmut@freexian.com>
