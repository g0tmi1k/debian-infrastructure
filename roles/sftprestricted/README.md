sftprestricted
==============

Configure openssh such that members of a particular group can only access sftp.

Role Variables
--------------

`sftprestricted_group` is the name of a unix group whose members will be
restricted to using sftp inside a chroot when they connect to ssh.

`sftprestricted_chroot_area` is the location of a chroot hierarchy. Each
connecting user will be chrooted to a subdirectory matching their login.

`sftprestricted_login` is a user name to be restricted by the `restrict_user`
task. It'll add the user to the restricted group and create its chroot
directory.

Example
-------

    - name: Enable restricted sftp
      ansible.builtin.include_role:
        name: sftprestricted

    - name: Confine hunter to restricted sftp
      ansible.builtin.include_role:
        name: sftprestricted:
	tasks_from: restrict_user
      vars:
        sftprestricted_login: hunter

License
-------

MIT

Author Information
------------------

Helmut Grohne <helmut@subdivi.de>
