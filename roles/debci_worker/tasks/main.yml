- name: Ensure presence of /etc/debci
  ansible.builtin.file:
    path: /etc/debci
    state: directory
    mode: "0755"

- name: Create debci-update-worker replacement
  ansible.builtin.template:
    dest: /etc/debci/update-worker
    src: update-worker.j2
    mode: "0755"

- name: Divert debci-update-worker
  community.general.dpkg_divert:
    path: /usr/share/debci/bin/debci-update-worker
    divert: /usr/share/debci/bin/debci-orig-update-worker
    rename: true

- name: Ensure presence of /usr/share/debci
  ansible.builtin.file:
    path: /usr/share/debci
    state: directory
    mode: "0755"

- name: Ensure presence of /usr/share/debci/bin
  ansible.builtin.file:
    path: /usr/share/debci/bin
    state: directory
    mode: "0755"

- name: Replace debci-update-worker
  ansible.builtin.file:
    path: /usr/share/debci/bin/debci-update-worker
    state: link
    src: /etc/debci/update-worker

- name: Install packages for the lxc backend
  when: "'lxc' in (debci_suites.values() | map(attribute='backend', default='lxc'))"
  ansible.builtin.apt:
    pkg:
      - apparmor # otherwise lxc-start fails to initialize containers
      - lxc-templates
    install_recommends: false

- name: Install packages for the qemu backend
  when: "'qemu' in (debci_suites.values() | map(attribute='backend', default='lxc'))"
  ansible.builtin.apt:
    pkg:
      - ksmtuned  # Reduce ram usage
      - qemu-kvm
      - qemu-user-static  # qemu-debootstrap is required by vmdb2 (<< 0.27) for sibling architectures
      - vmdb2
      - zerofree
    install_recommends: false

- name: Ensure presence of /etc/debci/conf.d
  ansible.builtin.file:
    path: /etc/debci/conf.d
    state: directory
    mode: "0755"

- name: Configure debci-worker
  ansible.builtin.template:
    dest: /etc/debci/conf.d/20-worker.conf
    src: worker.conf.j2
    mode: "0644"

- name: Install debci-worker
  ansible.builtin.apt:
    pkg: debci-worker
    install_recommends: false

- name: Add debci user to kvm group
  when: "'qemu' in (debci_suites.values() | map(attribute='backend', default='lxc'))"
  ansible.builtin.user:
    user: debci
    append: true
    groups: kvm
  notify: Restart debci-workers

- name: Increase qemu ram
  when: "'qemu' in (debci_suites.values() | map(attribute='backend', default='lxc'))"
  ansible.builtin.copy:
    dest: /etc/debci/conf.d/50-qemu_memory.conf
    src: qemu_memory.conf
    mode: "0644"
  notify: Restart debci-workers

- name: Disable default worker
  ansible.builtin.systemd:
    name: debci-worker@1.service
    enabled: false
    state: stopped

- name: Add worker wrapper
  ansible.builtin.copy:
    dest: /etc/debci/worker-instance
    src: worker-instance
    mode: "0755"
  notify: Restart debci-workers

- name: Create debci-worker@.service directory
  ansible.builtin.file:
    path: /etc/systemd/system/debci-worker@.service.d
    state: directory
    mode: "0755"
  notify: Restart debci-workers

- name: Tweak debci-worker@.service template
  ansible.builtin.copy:
    dest: /etc/systemd/system/debci-worker@.service.d/tweak-template.conf
    src: tweak-debci-worker-template.conf
    mode: "0644"
  notify: Restart debci-workers

- name: Enable debci-worker@.service instances
  ansible.builtin.systemd:
    name: >-
      debci-worker@{{ item.0.0.backend }}_{{ item.0.1 }}_{{ item.1 }}.service
    enabled: true
    state: started
  loop: >-
    {{ debci_suites.values() | list | subelements('architectures') | product(range(debci_worker_instances)) }}
