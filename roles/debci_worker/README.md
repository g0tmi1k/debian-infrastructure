debci\_worker
=============

Install and configure debci-worker. This includes the setup of workers
instances. The `debci-worker@*.service` is repurposed and one is created for
each configured combination of architecture, suite and backend.

This role assumes the presence of a working AMQP server. Either run the
`rabbitmq` role or configure a `debci_amqp_server` in the `debci_common` role.

Role Variables
--------------

The `debci_suites` variable defines the suites this worker tests on. For each
suite, the following keys may be defined:
 * `architectures` (mandatory) is a list of Debian architecture names to test.
 * `backend` is optional and defaults to `lxc`. The `qemu` backend is also
   supported.
 * `mirror` can be optionally specified to use a non-default mirror.
 * `keyring` can be optionally specified to use a keyring that is not trusted
   by the system apt.

`debci_worker_instances` defines the number of workers instances to set up for
each suite and architecture combination. This is not the total number of
workers, but to be multiplied with the number of suites and number of
architectures.

License
-------

MIT

Author Information
------------------
Sébastien Delafond <sdelafond@gmail.com>
Helmut Grohne <helmut@freexian.com>
