# Debusine-server role

The role takes care of almost everything for you. The remaining steps that
you have to handle are:

* configuring the HTTPS certificate management and the corresponding
  configuration entries in the virtual host (if you use HTTPS). The playbook
  can set Nginx for you if the variable `debusine_server_setup_nginx` is true.
  You will need to modify `/etc/nginx/sites-available/debusine.conf` to
  enable HTTPS for debusine-server
* Approve workers when they connect to the instance
 (see https://freexian-team.pages.debian.net/debusine/admin/first-steps.html#enable-a-new-worker-on-the-server-based-on-the-worker-s-name)
* Create users and tokens for the debusine clients
 (see https://freexian-team.pages.debian.net/debusine/admin/first-steps.html#creating-token-for-debusine-client)

# Role variables

## Variables

* `debusine_server_fqdn`: the fully qualified domain name used by the
  service (set the application and reverse proxy virtual hosts).
* `debusine_server_setup_nginx`: true/false. If true: set up nginx
* `debusine_use_snapshot_repository`: if true use the latest debusine devel build.
  Otherwise, use the debusine available on deb.freexian.com
* `debusine_mailserver`: not set, none or 'postfix'. If 'postfix' install postfix package. It does
  not set up Postfix. Debusine will try to use localhost to send notification emails
* `debusine_server_configuration`: Optional debusine server configuration in
  Python synaxt to be included in the `local.py`.

To manually set up nginx, set the variable `debusine_server_setup_nginx` to
`false`. In your nginx debusine server configuration, include the line:
```
include /etc/nginx/snippets/debusine.conf;
```

This configuration file is provided with the `debusine-server` package.

# Example playbook
```
---
- name: Install debusine-server
  hosts: debusine_server
  vars:
    debusine_server_fqdn: debusine.server.com
    debusine_server_setup_nginx: true
    debusine_use_snapshot_repository: true
  tasks:
    - name: Install debusine-server
      ansible.builtin.include_role:
        name: freexian.debian_infrastructure.debusine_server
```

# License

MIT

# Author Information

Carles Pina i Estany <carles@pina.cat>

