---
# Setup the packages
- name: Install the packages required for mirror-status
  ansible.builtin.apt:
    update_cache: true
    name:
      - alembic
      - postgresql
      - python3-alembic
      - python3-bs4
      - python3-dateutil
      - python3-jinja2
      - python3-psycopg2
      - python3-sqlalchemy

# Create a mirror-status user
- name: Create dedicated user
  ansible.builtin.user:
    name: mirror-status
    home: '{{ mirror_status_homedir }}'
    comment: 'Mirror Status User'
    create_home: true

# Setup the database
- name: Create the mirror-status postgresql role
  community.postgresql.postgresql_user:
    name: mirror-status
    role_attr_flags: CREATEDB,NOCREATEROLE
  become_user: postgres
  become: true

- name: Create the mirror-status database
  community.postgresql.postgresql_db:
    name: mirror-status
    owner: mirror-status
    encoding: UTF-8
    lc_ctype: en_US.UTF-8
    lc_collate: en_US.UTF-8
  become_user: postgres
  become: true

- name: Add the pgcrypto extension
  community.postgresql.postgresql_ext:
    db: mirror-status
    name: pgcrypto
  become_user: postgres
  become: true

# Setup the git repo
- name: Checkout the mirror-status git repository
  ansible.builtin.git:
    repo: '{{ mirror_status_git_url }}'
    dest: '{{ mirror_status_homedir }}/mirror-status'
    force: yes
    update: false   # updates might require db upgrade, hence done manually
  become_user: mirror-status
  become: true
  register: git_clone

- name: Initialize the database
  ansible.builtin.command:
    cmd: alembic upgrade head
    chdir: '{{ mirror_status_homedir }}/mirror-status'
  become_user: mirror-status
  become: true
  when:
    git_clone.changed

# Configure cron
- name: Configure cron to run on a periodic basis
  ansible.builtin.cron:
    user: mirror-status
    name: mirror-status-update
    minute: '*/20'
    job: '{{ mirror_status_homedir }}/mirror-status/{{ mirror_status_git_update_script }}'

# Configure the web part
- name: Configure nginx
  when: mirror_status_webserver == "nginx"
  notify: reload mirror-status webserver
  block:
    - name: Replace the server_name in nginx's vhost
      ansible.builtin.lineinfile:
        path: /etc/nginx/sites-available/{{ mirror_status_website_name }}
        backrefs: true
        regexp: '(\s)*server_name .*'
        line: '\g<1>server_name {{ mirror_status_fqdn }};'

    - name: Enable the virtual host
      ansible.builtin.file:
        path: /etc/nginx/sites-enabled/{{ mirror_status_website_name }}
        src: /etc/nginx/sites-available/{{ mirror_status_website_name }}
        state: link

- name: Configure apache2
  when: mirror_status_webserver == "apache2"
  notify: reload mirror-status webserver
  block:
    - name: Replace the server_name in apache's vhost
      ansible.builtin.lineinfile:
        path: /etc/apache2/sites-available/{{ mirror_status_website_name }}.conf
        backrefs: true
        regexp: '(\s)*ServerName .*'
        line: '\g<1>ServerName {{ mirror_status_fqdn }}'

    - name: Enable the virtual host
      ansible.builtin.file:
        path: /etc/apache2/sites-enabled/{{ mirror_status_website_name }}.conf
        src: /etc/apache2/sites-available/{{ mirror_status_website_name }}.conf
        state: link
