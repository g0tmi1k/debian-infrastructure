#!/usr/bin/python3
# SPDX-License-Identifier: MIT
"""Run recurrent tasks in systemd-nspawn containers via systemd units.

A task group is directory that contains a "servertasks.yaml". The name of the
directory becomes the task group's name. A container is being created for each
group and all member tasks share this one container.

Containers are application containers without an init system or services
managed using systemd-nspawn. Their read-only root filesystem is periodically
recreated using mmdebstrap and replaced atomically. All containers run with the
same subuid/subgid range and as the same user, but their view on processes and
files is restricted using relevant namespaces. Tasks running inside a container
can access some filesystem resources identified by environment variables named
*_DIRECTORY. Tasks are run with PR_SET_NO_NEW_PRIVS and all capabilities
dropped. Thus their chances of escaping from the filesystem container are
rather dim. Tasks can still exhaust resources and eleveate their privileges via
network resources and IPC mechanisms (e.g. via the loopback network interface).
"""

import argparse
import collections
import contextlib
import datetime
import errno
import fcntl
import hashlib
import logging
import os
import pathlib
import shlex
import shutil
import stat
import subprocess
import sys
import typing

import jsonschema
import yaml


TIME_FORMAT = "%Y%m%d%H%M%S"

GROUP_SCHEMA_TEXT = r"""---
title: Task group
description: >-
  Configure a group of dependent tasks that share a common set of dependencies, credentials and persistent state directory.
type: object
properties:
  suite:
    title: Debian suite
    description: >-
      The name of the Debian suite to bootstrap the container image from and to use for installing dependencies.
      All task commands will be run inside this chroot environment.
    type: string
    pattern: '^[a-z][a-z]+$'
    default: stable
    examples:
      - bookworm
      - trixie
      - forky

  dependencies:
    type: array
    items:
      title: Debian package
      description: >-
        The name of a Debian package to install in the container environment used for executing all of the tasks.
      type: string
      pattern: '^[a-z0-9][a-z0-9.+-]+$'
    default: []
    examples:
      - [python3]

  tasks:
    title: A set of tasks
    description: >-
      Each key is a task name and used for constructing a systemd.unit.
      The associated object describes the actual task at hand.
    type: object
    pattern: '^[a-z][a-z0-9_]*$'
    patternProperties:
      '.*':
        title: An individual task
        description: >-
          A task is a command to be run on regular schedule.
          For each task a corresponding systemd.service and systemd.path unit will be created.
          If a schedule is given, a systemd.timer unit is created as well.
          The actual task will be run in a systemd.nspawn container without a supervisor.
          A each task service may be activated by its associated timer unit or by having another task activate it by touching a state file monitored by the aforementioned path unit.
          Credentials are communicated via the CREDENTIALS_DIRECTORY environment variable.
          The persistent state directory is communicated via the DATA_DIRECTORY variable.
          A trigger directory containing a file for each task to be touched is communicated via the TRIGGER_DIRECTORY variable.
          Unless a task has sandboxed networking, it may send mail by contacting 127.0.0.1:25.
        type: object
        properties:
          credentials:
            title: Required credentials
            description: >-
              A set of credential names whose value is required for running the task.
              Note that each credential must be admitted to the task group to be accessible.
            type: array
            items:
              title: Credential name
              description: >-
                The name of a credential required for running this task.
                The credential shall be available from "${CREDENTIALS_DIRECTORY}/${name}" to the task.
                This is how systemd also handles credentials.
                The value of a credential is defined outside of the task definition.
              type: string
            uniqueItems: true
            default: []

          executable:
            title: Executable to run
            description: >-
              A relative path from the location of the task configuration to the executable.
              It should run without any arguments.
              If arguments are needed, consider adding a wrapping script.
            type: string
            pattern: '^(?!\.|/|.*/\.\./)[A-Za-z0-9._/-]+$'

          schedule:
            title: When the task should be run
            description: >-
              A systemd.time(5) calendar event expression specifying when to run the timer.
            oneOf:
              - type: string
                pattern: '^[A-Za-z0-9 .,:/~*-]+$'
              - type: "null"
            default: null

          sandbox:
            title: Sandboxing mechanisms
            description: >-
              Configure sandboxing mechanisms
            type: object
            properties:
              networknamespace:
                title: Private network namespace
                description: >-
                  Enabling this will cause PrivateNetwork to be set to true and thus the task will be unable to access the internet.
                type: boolean
                default: false
        required:
          - executable
    default: []
"""

GROUP_CONFIG_FILE = "servertasks.yaml"

logger = logging.getLogger("servertasks")


def validate_systemd_calendar(calendarspec: str) -> None:
    """Check whether the given calendarspec is a valid systemd.time value for
    use with OnCalendar.

    >>> validate_systemd_calendar("daily")
    >>> validate_systemd_calendar("1d")
    Traceback (most recent call last):
    ...
    ValueError: not a valid systemd.time calendar value
    >>>
    """
    try:
        subprocess.check_call(
            ["systemd-analyze", "calendar", calendarspec],
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )
    except subprocess.CalledProcessError as err:
        raise ValueError("not a valid systemd.time calendar value") from err


def quote_systemd(value: str) -> str:
    r"""Quote a value for use as an item in ExecStart.
    >>> quote_systemd('hello')
    'hello'
    >>> quote_systemd('percent % dollar $ quote "')
    '\'percent %% dollar $$ quote "\''
    >>> quote_systemd("' \" '")
    '"\' \\" \'"'
    """
    if value == ";":
        return "\\;"
    value = value.translate(
        {ord("$"): "$$", ord("%"): "%%", ord("\\"): r"\\", ord("\n"): r"\n"}
    )
    if any(c.isspace() for c in value):
        quote = min(("'", '"'), key=value.count)
        value = quote + value.translate({ord(quote): "\\" + quote}) + quote
    return value


def gen_timesuffix() -> str:
    """Generate a token that expresses the current time."""
    return datetime.datetime.now().strftime(TIME_FORMAT)


def timesuffix_older(suffix: str, delta: datetime.timedelta) -> bool:
    """Check whether a time token is older than the given delta.

    >>> timesuffix_older(gen_timesuffix(), datetime.timedelta(seconds=-1))
    True
    >>> timesuffix_older(gen_timesuffix(), datetime.timedelta(hours=1))
    False
    """
    suffixtime = datetime.datetime.strptime(suffix, TIME_FORMAT)
    return suffixtime + delta < datetime.datetime.now()


def run_command(argv: list[str], noact: bool) -> None:
    """Run the given command. The command is logged at info level in
    shell-compatible syntax. If noact is enabled, execution is skipped.
    """
    logger.info("%s", shlex.join(argv))
    if not noact:
        subprocess.check_call(argv)


def make_symlink(
    target: pathlib.Path, linkpath: pathlib.Path, noact: bool
) -> None:
    """Create a relative symbolic link at linkpath pointing to target.
    When the target path is relative, it is interpreted as relative to the
    directory containing linkpath. If the resulting target points below that
    directory, the target is made relative.
    """
    # Leaves target unchanged if it is absolute.
    target = linkpath.parent / target
    try:
        target = target.relative_to(linkpath.parent)
    except ValueError:
        pass
    exists = True
    try:
        if linkpath.readlink() == str(target):
            return
    except FileNotFoundError:
        exists = False
    logger.info("%s", shlex.join(["ln", "-sf", str(target), str(linkpath)]))
    if not noact:
        if exists:
            linkpath.unlink()
        linkpath.symlink_to(target)


def make_directory(dirpath: pathlib.Path, uidgid: int, noact: bool) -> None:
    """Create a directory. Its group and owner are set to the given uidgid.
    Equivalent shell commands are logged. If noact is enabled, the actual
    creation is inhibited.
    """
    try:
        statres = dirpath.lstat()
    except FileNotFoundError:
        if uidgid:
            logger.info(
                "install -d --owner=%s --group=%s %s",
                uidgid,
                uidgid,
                shlex.quote(str(dirpath)),
            )
        else:
            logger.info("mkdir %s", shlex.quote(str(dirpath)))
        if not noact:
            dirpath.mkdir()
            if uidgid:
                os.chown(dirpath, uidgid, uidgid)
    else:
        if not stat.S_ISDIR(statres.st_mode):
            raise OSError(errno.ENOTDIR, f"not a directory: {dirpath!r}")
        if statres.st_uid != uidgid or statres.st_gid != uidgid:
            logger.info(
                "chown %s:%s %s", uidgid, uidgid, shlex.quote(str(dirpath))
            )
            if not noact:
                os.chown(dirpath, uidgid, uidgid)


def remove(path: pathlib.Path, noact: bool, recursive: bool = False) -> None:
    """Remove a path from the filesystem optionally recursively. A
    shell-compatible invocation is logged. If noact is enabled, the actual
    removal is inhibited.
    """
    if recursive:
        logger.info("rm -R %s", shlex.quote(str(path)))
        if not noact:
            shutil.rmtree(path)
    else:
        logger.info("rm %s", shlex.quote(str(path)))
        if not noact:
            path.unlink()


def allowed_credentials(args: argparse.Namespace, groupname: str) -> set[str]:
    """Collect the credentials that the given group has been allowed to use."""
    prefix = groupname + ":"
    result = set()
    for entry in args.allow_credential:
        if entry.startswith(prefix):
            result.add(entry[len(prefix) :])
    return result


# As a first approximation, model a TaskGroupConfig as a dict. In principle,
# a TypedDict would be better, but it would largely duplicate the jsonschema.
TaskGroupConfig = dict[str, typing.Any]


def validate_group(group: TaskGroupConfig) -> None:
    """Verify validity of the given group definition using the json schema and
    some more in-depth checks.
    """
    jsonschema.validate(group, yaml.safe_load(GROUP_SCHEMA_TEXT))
    for name, task in group.get("tasks", {}).items():
        schedule = task.get("schedule")
        if schedule:
            try:
                validate_systemd_calendar(schedule)
            except ValueError as err:
                raise ValueError(f"{err} used for task {name}") from err


def validate_group_credentials(
    group: TaskGroupConfig, valid_credentials: set[str]
) -> None:
    """Verify that the given group only requires credentials from the given
    valid_credentials. The group should be validated before calling this.
    """
    for name, task in group.get("tasks", {}).items():
        not_allowed = set(task.get("credentials", [])) - valid_credentials
        if not_allowed:
            not_allowed_str = ", ".join(map(repr, not_allowed))
            raise ValueError(
                f"credentials {not_allowed_str} not allowed for task {name}"
            )


def load_group_config(grouppath: pathlib.Path) -> TaskGroupConfig:
    """Load a group configuration from a yaml file and validate it."""
    configpath = grouppath / GROUP_CONFIG_FILE
    if configpath.is_symlink():
        raise ValueError(f"{GROUP_CONFIG_FILE} must not be a symbolic link")
    with configpath.open() as fobj:
        config = yaml.safe_load(fobj)
    assert isinstance(config, dict)
    validate_group(config)
    return config


def hash_container(groupconfig: TaskGroupConfig) -> str:
    """Compute a hash that changes when the container configuration changes."""
    # The container image only depends on suite and dependencies and _ is a
    # separator that cannot show up any of them.
    tohash = "_".join(
        [groupconfig.get("suite", "")]
        + sorted(groupconfig.get("dependencies", []))
    )
    return hashlib.sha1(tohash.encode("utf8")).hexdigest()


def load_subidfile(
    subidpath: pathlib.Path
) -> typing.Iterable[tuple[str, int, int]]:
    """Parse a /etc/sub?id file and return tuples of (user, start, count).
    """
    for linenr, line in enumerate(
        subidpath.read_text(encoding="ascii").splitlines()
    ):
        parts = line.strip().split(":")
        if len(parts) != 3:
            raise ValueError(
                f"invalid line {subidpath}:{linenr}: require exactly 2 colons"
            )
        try:
            start = int(parts[1])
            count = int(parts[2])
        except ValueError as err:
            raise ValueError(
                f"invalid line {subidpath}:{linenr}: cannot parse ids"
            ) from err
        yield (parts[0], start, count)


@contextlib.contextmanager
def locked_base_dir(args: argparse.Namespace) -> typing.Iterator[None]:
    """Obtain flock-based exclusive lock on the file flock in the base_dir for
    the duration of the context.
    """
    with (args.base_dir / "flock").open("wb") as lockfile:
        fcntl.flock(lockfile, fcntl.LOCK_EX)
        try:
            yield None
        finally:
            fcntl.flock(lockfile, fcntl.LOCK_UN)


def systemctl_show(pattern: str) -> typing.Iterator[dict[str, str]]:
    """Wrapper for systemctl show pattern. It parses the output into a dict of
    assignments for each matched unit.

    >>> any(systemctl_show("nbCZVfTfHRM3-*"))
    False
    >>> next(systemctl_show("-.mount"))["Where"]
    '/'
    """
    unit = {}
    for line in subprocess.check_output(
        ["systemctl", "show", "--", pattern], encoding="utf8"
    ).splitlines():
        if "=" in line:
            key, value = line.split("=", 1)
            unit[key] = value
        elif line:
            raise ValueError(
                f"unexpected non-assignment line {line!r} from systemctl"
            )
        else:
            yield unit
            unit = {}
    if unit:
        yield unit


def do_init(args: argparse.Namespace) -> None:
    """Set up state directories and systemd generator"""
    for subdir in (".", "code", "chroots", "data", "groupdata", "triggers"):
        entry = args.base_dir / subdir
        if not entry.is_dir():
            make_directory(entry, 0, args.noact)

    for kind in "ug":
        subidfile = pathlib.Path(f"/etc/sub{kind}id")

        found = False
        for user, start, count in load_subidfile(subidfile):
            if user not in ("0", "root"):
                continue
            if (
                start <= args.subid_start
                and start + count >= args.subid_start + 65536
            ):
                found = True
                break

        if not found:
            logger.info(
                "echo root:%s:65536 >> %s", args.subid_start, subidfile
            )
            if not args.noact:
                with subidfile.open(mode="a", encoding="ascii") as fobj:
                    fobj.write(f"root:{args.subid_start}:65536\n")
    generator_path = pathlib.Path("/etc/systemd/system-generators")
    make_directory(generator_path, 0, args.noact)
    generator_path /= "servertasks"
    if not generator_path.is_file():
        logger.info("Creating %s", generator_path)
        if not args.noact:
            mypath = pathlib.Path(__file__)
            generator_path.write_text(
                f"""#!/bin/sh
exec python3 {shlex.quote(str(mypath))} generator "$1"
"""
            )
            generator_path.chmod(0o755)


def do_validate(args: argparse.Namespace) -> None:
    """Validate a given code directory and its yaml"""
    config = load_group_config(args.group)
    validate_group_credentials(
        config, allowed_credentials(args, args.group.name)
    )


def do_install(args: argparse.Namespace) -> None:
    """Install or upgrade a given code directory"""
    with locked_base_dir(args):
        groupname = args.group.name
        assert "-" not in groupname
        config = load_group_config(args.group)
        validate_group_credentials(
            config, allowed_credentials(args, groupname)
        )
        curcode = args.base_dir / "code" / groupname
        newcode = args.base_dir / "code" / (groupname + "-" + gen_timesuffix())
        run_command(["cp", "-dR", str(args.group), str(newcode)], args.noact)
        make_symlink(newcode, curcode, args.noact)
        make_directory(args.base_dir / "data" / groupname, 0, args.noact)
        make_directory(
            args.base_dir / "groupdata" / groupname,
            args.subid_start + args.inner_uid,
            args.noact,
        )
        make_directory(args.base_dir / "triggers" / groupname, 0, args.noact)
        for task in config.get("tasks", {}):
            make_directory(
                args.base_dir / "data" / groupname / task,
                args.subid_start + args.inner_uid,
                args.noact,
            )
            trigger_file = args.base_dir / "triggers" / groupname / task
            if trigger_file.is_file():
                continue
            run_command(
                [
                    "install",
                    f"--owner={args.subid_start + args.inner_uid}",
                    f"--group={args.subid_start + args.inner_uid}",
                    "-m600",
                    "/dev/null",
                    str(trigger_file),
                ],
                args.noact,
            )


def bootstrap_container(
    args: argparse.Namespace, target: pathlib.Path, config: TaskGroupConfig
) -> None:
    """Actually create the container using mmdebstrap into the target path
    according to the given group configuration.
    """
    # We have a workaround for systemd-nspawn credential passing options and
    # --user= being incompatible. This workaround requires mount.
    dependencies = {"mount"}
    dependencies.update(config.get("dependencies", set()))

    mount_points = [
        args.inner_base_dir / subdir
        for subdir in "code data groupdata triggers".split()
    ]

    mmdebstrap_cmd = [
        "unshare",
        "--user",
        "--mount",
        "--uts",
        "--ipc",
        "--pid",
        f"--map-users={args.subid_start},0,65536",
        f"--map-groups={args.subid_start},0,65536",
        "--setuid=0",
        "--setgid=0",
        "--fork",
        "mmdebstrap",
        config.get("suite", "stable"),
        str(target),
        "--mode=root",
        "--variant=essential",
        '--customize-hook=useradd "--root=$1" ' + shlex.join(
            [
                "--home-dir=" + str(args.inner_base_dir / "data"),
                "--no-create-home",
                "--user-group",
                f"--uid={args.inner_uid}",
                args.inner_user,
            ],
        ),
        '--customize-hook=mkdir -p ' + " ".join(
            f'"$1{mount_point}"' for mount_point in mount_points
        ),
        "--include=" + ",".join(dependencies),
    ]

    make_directory(target, args.subid_start, args.noact)
    run_command(mmdebstrap_cmd, args.noact)


def do_bootstrap(args: argparse.Namespace) -> None:
    """Create or upgrade containers for all installed task groups"""
    with locked_base_dir(args):
        for group in (args.base_dir / "code").iterdir():
            # We'll have one "current" symlink for each group.
            if not group.is_symlink():
                continue
            config = load_group_config(group)
            configsuffix = hash_container(config)

            chroot_dir = args.base_dir / "chroots"
            curchroot = chroot_dir / configsuffix

            try:
                oldchroot = curchroot.readlink()
            except FileNotFoundError:
                pass
            else:
                # The chroot name encodes when it was created in the
                # timesuffix. Thus we may check whether it has expired
                # according to --bootstrap-interval and only then recreate it.
                oldconfigsuffix, oldtimesuffix = oldchroot.name.split("-")
                if oldconfigsuffix == configsuffix and not timesuffix_older(
                    oldtimesuffix, args.bootstrap_interval
                ):
                    continue

            newchroot = chroot_dir / f"{configsuffix}-{gen_timesuffix()}"
            bootstrap_container(args, newchroot, config)
            make_symlink(newchroot, curchroot, args.noact)


def generate_units(
    args: argparse.Namespace, groupdir: pathlib.Path
) -> typing.Iterator[tuple[str, str]]:
    """Generate systemd units and return pairs of unit name and their content.
    """
    groupname, _ = groupdir.name.split("-")
    config = load_group_config(groupdir)
    validate_group_credentials(config, allowed_credentials(args, groupname))
    configsuffix = hash_container(config)

    for taskname, task in config.get("tasks", {}).items():
        unitbase = f"servertasks-{groupname}-{taskname}"
        nspawn_cmd = [
            "systemd-nspawn",
            "--keep-unit",
            f"--directory={args.base_dir}/chroots/{configsuffix}",
            "--read-only",
            "--as-pid2",
            "--no-new-privileges=yes",
            "--tmpfs=/tmp:nosuid,nodev",
            "--kill-signal=SIGTERM",
        ]
        for evar, subdir, location in (
            ("CODE_DIRECTORY", "code", f"code/{groupdir.name}"),
            ("DATA_DIRECTORY", "data", f"data/{groupname}/{taskname}"),
            ("TRIGGER_DIRECTORY", "triggers", "triggers/" + groupname),
            ("GROUPDATA_DIRECTORY", "groupdata", "groupdata/" + groupname),
        ):
            target = args.inner_base_dir / subdir
            nspawn_cmd.append(f"--setenv={evar}={target}")
            nspawn_cmd.append(f"--bind={args.base_dir}/{location}:{target}")
        for cred in task.get("credentials", []):
            nspawn_cmd.append(
                f"--load-credential={cred}:${{CREDENTIALS_DIRECTORY}}/{cred}"
            )
        if task.get("sandbox", {}).get("networknamespace"):
            nspawn_cmd.append("--private-network")
        else:
            nspawn_cmd.append("--resolv-conf=bind-host")

        executable = args.inner_base_dir / "code" / task["executable"]
        # Unfortunately, --user= does not work in combination with passing
        # credentials, because the user will be unable to read credentials.
        # To work around this, we temporarily need to mount the credential
        # directory read-write requiring the mount command in the container
        # and also requiring us to retain capabilities initially. We then use
        # setpriv to implement --user= and --drop-capability=all.
        if task.get("credentials"):
            script_cmds = [
                "set -eu",
                'mount "$CREDENTIALS_DIRECTORY" -wo remount',
                (
                    f"chown -R '{args.inner_user}:{args.inner_user}' "
                    '"$CREDENTIALS_DIRECTORY"'
                ),
                'mount "$CREDENTIALS_DIRECTORY" -ro remount',
                shlex.join(
                    [
                        "exec",
                        "setpriv",
                        "--no-new-privs",
                        "--reuid",
                        args.inner_user,
                        "--regid",
                        args.inner_user,
                        "--clear-groups",
                        "--inh-caps=-all",
                        str(executable),
                    ],
                ),
            ]
            nspawn_cmd.extend(
                ["sh", "-c", quote_systemd("; ".join(script_cmds))]
            )
        else:
            nspawn_cmd.extend(
                [
                    "--user=" + args.inner_user,
                    "--drop-capability=all",
                    quote_systemd(str(executable)),
                ],
            )
        service = f"""[Unit]
SourcePath={args.base_dir}/code/{groupname}-{configsuffix}/{GROUP_CONFIG_FILE}
[Service]
Type=oneshot
RemainAfterExit=no
ExecStart={" ".join(nspawn_cmd)}
DevicePolicy=closed
KillMode=mixed
TimeoutStartSec={args.task_timeout.total_seconds()}
"""
        service += "".join(
            f"LoadCredential={cred}\n" for cred in task.get("credentials", [])
        )
        yield (unitbase + ".service", service)
        yield (
            unitbase + ".path",
            f"""[Unit]
SourcePath={args.base_dir}/code/{groupname}-{configsuffix}/{GROUP_CONFIG_FILE}
StopWhenUnneeded=yes
[Path]
PathChanged={args.base_dir}/triggers/{groupname}/{taskname}
""",
        )
        if task.get("schedule"):
            yield (
                unitbase + ".timer",
                f"""[Unit]
SourcePath={args.base_dir}/code/{groupname}-{configsuffix}/{GROUP_CONFIG_FILE}
StopWhenUnneeded=yes
[Timer]
OnCalendar={task["schedule"]}
""",
            )


def do_generator(args: argparse.Namespace) -> None:
    """Generate systemd units for all installed task groups"""
    wants = set()
    for codedir in (args.base_dir / "code").iterdir():
        if not codedir.is_symlink():
            continue
        codedir = codedir.parent / codedir.readlink()
        for uname, content in generate_units(args, codedir):
            logger.info("Creating %s", args.output / uname)
            if not args.noact:
                (args.output / uname).write_text(content)
            if not uname.endswith(".service"):
                wants.add(uname)
    if wants:
        logger.info("Creating %s/servertasks.target", args.output)
        if not args.noact:
            (args.output / "servertasks.target").write_text(
                "[Unit]\n" + "".join(f"Wants={unit}\n" for unit in wants)
            )


def do_update(args: argparse.Namespace) -> None:
    """Update containers, regenerate units and activate them"""
    do_bootstrap(args)
    # Stopping the target should cause all wanted units to be stopped before
    # we clear their StopWhenUnneeded via systemctl reload.
    if subprocess.call(
        ["systemctl", "is-active", "--quiet", "servertasks.target"]
    ) == 0:
        run_command(["systemctl", "stop", "servertasks.target"], args.noact)
    run_command(["systemctl", "daemon-reload"], args.noact)
    # systemd does not automatically stop units whose unit file has vanished
    # nor does StopWhenUnneeded=yes do this reliably.
    for unit in systemctl_show("servertasks-*"):
        if unit.get("LoadState") == "not-found":
            run_command(["systemctl", "stop", unit["Id"]], args.noact)
        if unit.get("ActiveState") == "failed":
            run_command(["systemctl", "reset-failed", unit["Id"]], args.noact)
    if any(
        subdir.is_symlink() for subdir in (args.base_dir / "code").iterdir()
    ):
        action = "restart"
    elif subprocess.call(
        ["systemctl", "is-active", "--quiet", "servertasks.target"]
    ) != 0:
        return
    else:
        action = "stop"
    run_command(["systemctl", action, "servertasks.target"], args.noact)


def do_cleanup(args: argparse.Namespace) -> None:
    """Remove expired code and chroot directories"""
    with locked_base_dir(args):
        # First iterate through code directories and locate expired ones.
        # For each non-expired code directory, remember the container hash
        # for cleaning up chroots later.
        container_hashes = set()
        keep = set()
        for subdir in sorted((args.base_dir / "code").iterdir(), reverse=True):
            if subdir.is_symlink():
                continue
            groupname, timesuffix = subdir.name.split("-")
            if not timesuffix_older(timesuffix, args.task_timeout):
                # A task may still be running.
                container_hashes.add(hash_container(load_group_config(subdir)))
                continue
            try:
                current = (args.base_dir / "code" / groupname).readlink()
            except FileNotFoundError:
                # The task may still be running in principle, but it already
                # has been removed, so we remove anyway.
                remove(subdir, args.noact, recursive=True)
                continue
            if groupname not in keep or current == subdir.name:
                # Either the code has been updated within the task timeout and
                # we are looking at the previous code due to reversed sorting
                # or we are looking at the current code.
                keep.add(groupname)
                container_hashes.add(hash_container(load_group_config(subdir)))
            else:
                remove(subdir, args.noact, recursive=True)

        # Iterate through containers and remove symlinks referencing container
        # hashes that are no longer referenced from code remembering
        # directories for another pass.
        chrootdirs = set()
        for subdir in list((args.base_dir / "chroots").iterdir()):
            if subdir.name.startswith(".#") and subdir.name.endswith(".lck"):
                pass  # systemd-nspawn creates these.
            elif not subdir.is_symlink():
                chrootdirs.add(subdir)
            elif subdir.name not in container_hashes:
                remove(subdir, args.noact)

        # Now iterate through actual chroot directories deleting all that no
        # longer have a code reference as well as expired ones.
        keep = set()
        for subdir in sorted(chrootdirs, reverse=True):
            configsuffix, timesuffix = subdir.name.split("-")
            if not timesuffix_older(timesuffix, args.task_timeout):
                # A task may still be running.
                continue
            try:
                current = (args.base_dir / "chroots" / configsuffix).readlink()
            except FileNotFoundError:
                remove(subdir, args.noact, recursive=True)
                continue
            if configsuffix not in keep or current == subdir.name:
                # Either the chroot has been updated within the task timeout
                # and we are looking at the previous chroot due to reversed
                # sorting or we are looking at the current chroot.
                keep.add(configsuffix)
            else:
                remove(subdir, args.noact, recursive=True)

        # Finally remove left-over data and trigger entries from task groups
        # deleted via do_remove.
        for name in ("data", "groupdata", "triggers"):
            for subdir in list((args.base_dir / name).iterdir()):
                if (args.base_dir / "code" / subdir.name).is_symlink():
                    continue
                if any(systemctl_show(f"servertasks-{subdir.name}-*")):
                    # Don't delete data when units still exist.
                    continue
                remove(subdir, args.noact, recursive=True)


def do_remove(args: argparse.Namespace) -> None:
    """Remove a task group"""
    with locked_base_dir(args):
        grouplink = args.base_dir / "code" / args.group
        if not grouplink.is_symlink():
            raise ValueError(f"group {args.group!r} does not exist")
        remove(grouplink, args.noact)
        # Only remove the code link here as a task may still be running.
        # The cleanup command will eventually remove everything else.


def existing_directory(value: str) -> pathlib.Path:
    """A coercion function that casts a string into a Path and verifies that it
    refers to an existing directory.

    >>> isinstance(existing_directory("/"), pathlib.Path)
    True
    >>> existing_directory("nbCZVfTfHRM3")
    Traceback (most recent call last):
    ...
    ValueError: 'nbCZVfTfHRM3' is not a directory
    """
    location = pathlib.Path(value)
    if not location.is_dir():
        # Raise valueError instead of OSError to fit with the argparse API.
        raise ValueError(f"{value!r} is not a directory")
    return location


def group_directory(value: str) -> pathlib.Path:
    """A coercion function that casts a string into a Path and verifies that it
    is a plausible group directory."""
    location = existing_directory(value)
    if not all(c.isalnum() or c == "_" for c in location.name):
        raise ValueError(
            "group directory name must be alphanumeric with underscores"
        )
    if not (location / GROUP_CONFIG_FILE).exists():
        raise ValueError(f"no {GROUP_CONFIG_FILE} found in {location!r}")
    return location


def absolute_pure_path(value: str) -> pathlib.PurePath:
    """A coercion function that casts a string into a PurePath and verifies
    that it is absolute.

    This is used for paths rooted in a container. In order to access filesystem
    objects, the container root needs to be prepended or the path needs to be
    examined after having chroot()ed.
    """
    location = pathlib.PurePath(value)
    if not location.is_absolute():
        raise ValueError(f"{value!r} is not an absolute path")
    return location


def duration(value: str) -> datetime.timedelta:
    """Parse a textual representation of a duration inspired by systemd.time(7)
    into a timedelta.

    >>> duration("3 days")
    datetime.timedelta(days=3)
    >>> duration("2h")
    datetime.timedelta(seconds=7200)
    >>> duration("3 min 1second")
    datetime.timedelta(seconds=181)
    >>> duration("123")
    datetime.timedelta(seconds=123)
    """
    if not value.strip():
        raise ValueError("cannot parse empty duration")
    # Order is important and Python dicts retain order.
    scales = {
        "days": 3600 * 24,
        "day": 3600 * 24,
        "hours": 3600,
        "hour": 3600,
        "hr": 3600,
        "minutes": 60,
        "minute": 60,
        "min": 60,
        "seconds": 1,
        "second": 1,
        "sec": 1,
        "d": 3600 * 24,
        "h": 3600,
        "m": 60,
        "s": 1,
        "": 1,
    }
    result = 0
    parts = value.split()
    while parts:
        part = parts.pop(0)
        if part.isdigit() and parts and parts[0] in scales:
            result += scales[parts.pop(0)] * int(part)
            continue
        for suffix, factor in scales.items():
            if part.endswith(suffix):
                result += factor * int(part.removesuffix(suffix))
                break
        else:
            assert False, "unreachable code"
    return datetime.timedelta(seconds=result)


def search_subid_range(count: int = 65536) -> int:
    """Search for a subid range allocated to us or failing that unallocated and
    return its base.
    """
    # Map a base id to the id kinds where the chunk is allocated to us.
    candidates: dict[int, set[str]] = collections.defaultdict(set)
    # Map base ids to their allocation size. This may be overlapping if subuid
    # and subgid are out of sync.
    allocated: dict[int, int] = {}
    for kind in "ug":
        for user, start, cnt in load_subidfile(
            pathlib.Path(f"/etc/sub{kind}id")
        ):
            # Each line represents is an allocation of cnt ids to the user.
            if user in ("0", "root") and cnt >= count:
                candidates[start].add(kind)
            allocated[start] = max(cnt, allocated.get(start, 0))
    # Usually allocations in subuid and subgid are synced and we are searching
    # for such a synced allocation of at least count ids.
    for start, kinds in candidates.items():
        if kinds == set("ug"):
            return start
    # As we did not find a range, look for a sufficiently large chunk in the
    # unallocated space. Start searching behind the system range.
    start = 65536
    while allocated:
        next_start = min(allocated)
        if start + count <= next_start:
            break
        start = next_start + allocated.pop(next_start)
    return start


class CustomArgumentParser(argparse.ArgumentParser):
    """Tweak reading of @config files compared to ArgumentParser."""

    def convert_arg_line_to_args(self, arg_line: str) -> list[str]:
        """Tweak parsing of @config files to support comments, empty lines and
        simple assignments.
        """
        arg_line = arg_line.strip()
        if arg_line.startswith("#") or not arg_line:
            return []  # support comments
        if not arg_line.startswith("-"):
            return ["--" + arg_line]
        return [arg_line]


def add_command(
    subparser: "argparse._SubParsersAction[CustomArgumentParser]",
    command: str
) -> argparse.ArgumentParser:
    """Add a command to the given parser expecting a global function named
    f"do_{command}" and use its doc-string as help text.
    """
    function = globals()[f"do_{command}"]
    parser = subparser.add_parser(command, help=function.__doc__)
    parser.set_defaults(func=function)
    return parser


def main() -> None:
    """Main entry point doing command line parsing."""
    handler = logging.StreamHandler(sys.stdout)
    logger.addHandler(handler)
    parser = CustomArgumentParser(
        description=(
            "Run recurrent tasks in systemd-nspawn containers via systemd "
            "units."
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        fromfile_prefix_chars="@",
    )
    parser.add_argument(
        "-a",
        "--allow-credential",
        metavar="GROUP:CREDENTIAL",
        default=[],
        action="append",
        help="allow a group to use a credential",
    )
    parser.add_argument(
        "-b",
        "--base-dir",
        metavar="DIR",
        default=pathlib.Path("/var/local/servertasks"),
        type=existing_directory,
        help="location for storing all state, code and chroots",
    )
    parser.add_argument(
        "--bootstrap-interval",
        metavar="DURATION",
        default=datetime.timedelta(days=1),
        type=duration,
        help="regenerate chroots older than this",
    )
    parser.add_argument(
        "--inner-base-dir",
        metavar="DIR",
        type=absolute_pure_path,
        default=pathlib.PurePath("/srv/servertasks"),
        help="location inside chroots where task-specific data is mounted",
    )
    parser.add_argument(
        "--inner-uid",
        metavar="ID",
        type=int,
        default=10000,
        help="user id relative to --subid-start inside chroots to run tasks",
    )
    parser.add_argument(
        "--inner-user",
        metavar="NAME",
        type=str,
        default="servertasks",
        help="name of the user to use inside chroots to run tasks",
    )
    parser.add_argument(
        "-n",
        "--noact",
        action="store_true",
        help="do not actually perform any changes",
    )
    parser.add_argument(
        "--subid-start",
        metavar="ID",
        type=int,
        default=search_subid_range(),
        help="first ID for a subuid and subgid range of size 65536 to use",
    )
    parser.add_argument(
        "--task-timeout",
        metavar="DURATION",
        type=duration,
        default=datetime.timedelta(days=1),
        help="maximum time a task may run before being killed",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="print every action before performing it",
    )
    subparsers = parser.add_subparsers(required=True)
    add_command(subparsers, "init")
    add_command(subparsers, "validate").add_argument(
        "group",
        type=group_directory,
        help=f"a code directory containing a {GROUP_CONFIG_FILE}",
    )
    add_command(subparsers, "install").add_argument(
        "group",
        type=group_directory,
        help=f"a code directory containing a {GROUP_CONFIG_FILE}",
    )
    add_command(subparsers, "bootstrap")
    add_command(subparsers, "generator").add_argument(
        "output",
        type=existing_directory,
        help="a directory to write systemd units to",
    )
    add_command(subparsers, "update")
    add_command(subparsers, "cleanup")
    add_command(subparsers, "remove").add_argument(
        "group",
        type=str,
        help="name of a previously installed task group",
    )
    args = parser.parse_args()
    logger.setLevel(logging.INFO if args.verbose else logging.WARNING)
    args.func(args)


if __name__ == "__main__":
    main()
