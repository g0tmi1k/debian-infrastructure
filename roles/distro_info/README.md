Distro Info
===========

This role exports information about Debian-based distributions. It can be
used to retrieve information about the distributions:

- What's the repository URL for debian bookworm, or kali-rolling?
- What components are available in Ubuntu hirsute?
- What package contains the GPG keyring to authenticate the repositories?
- etc.

Calling this role, or including it as a dependency of another role, will
implicitly load the documented variable as well as the lookup plugin described
below, and you will be able to use them in your playbook.

Role Variables
--------------

All the information is stored in a giant dictionary stored in the
`distro_info` variable.

You can override the URL for the default mirror associated to each
vendor with the following variables (see defaults/main.yml):

```
distro_info_debian_default_mirror: "http://deb.debian.org/debian"
distro_info_debian_security_mirror: "http://security.debian.org"
distro_info_debian_archive_mirror: "http://archive.debian.org/debian"
distro_info_debian_archive_security_mirror: "http://archive.debian.org/debian-security"

distro_info_freexian_default_mirror: "http://deb.freexian.com/extended-lts"

distro_info_kali_default_mirror: "http://http.kali.org"
distro_info_kali_archive_mirror: "http://old.kali.org/kali"

distro_info_ubuntu_default_mirror: "http://archive.ubuntu.com/ubuntu"
distro_info_ubuntu_archive_mirror: "http://old-releases.ubuntu.com/ubuntu"
```

Lookup Plugin
-------------

The role contains a lookup plugin that lets you retrieve all the relevant
information for a given distribution with
`{{ lookup('freexian.debian_infrastructure.distro_info', distro_name) }}`.
The lookup returns a dict that looks like this:

    {
        'vendor': 'debian',
        'codename': 'bullseye',
        'repository': {
            'mirror': 'http://deb.debian.org/debian',
            'components': ['main', 'contrib', 'non-free'],
            'keyring_package': 'debian-archive-keyring',
            'keyring_file': '/usr/share/keyrings/debian-archive-keyring.gpg',
        },
        'debootstrap_options': '',
        'aliases': ['stable'],
        'parent': None,
        'binary_apt_entry': 'deb http://deb.debian.org/debian bullseye main contrib non-free',
        'source_apt_entry': 'deb-src http://deb.debian.org/debian bullseye main contrib non-free',
        'mergedusr': {
            'buildd' False,
            'default': True,
        },
    }

Example Playbook
----------------

    - hosts: servers
      roles:
        - role: freexian.distro_info
      tasks:
        - ansible.builtin.debug:
	      msg: "Info about kali-rolling distro: {{ lookup('freexian.debian_infrastructure.distro_info', 'kali-rolling') }}"
        - ansible.builtin.debug:
	      msg: "sources.list entry:: {{ lookup('freexian.debian_infrastructure.distro_info', 'ubuntu:focal').binary_apt_entry }}"

License
-------

MIT

Author Information
------------------

Raphaël Hertzog <raphael@freexian.com>
