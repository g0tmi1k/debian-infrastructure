# sbuild

## Role variables

There are many variables to configure this role but you can get it going
with something as simple as this:

```
sbuild_distributions:  # Required to specify the build chroots to build
  - unstable
  - debian:bullseye
  - kali:kali-dev

sbuild_architectures:  # Not required if you only need chroots for your host architecture
  - amd64
  - i386
```

If you need more fine-grained control of the chroots to build, you can
provide something like this:
```
sbuild_distributions:
  - distribution: bullseye
    architecture: amd64
    # Enable more repositories in the build chroot's sources.list file
    extra_distributions:
      - bullseye-security
      - bullseye-updates
    # Install supplementary packages in the build chroot
    extra_packages:
      - ccache
    # Add supplementary aliases in schroot configuration
    extra_aliases:
      - bullseye-test

  - distribution: buster

  - distribution: sid
    extra_aliases:
      - "foobar-{arch}-sbuild"
    allowed_architectures:
      - i386

sbuild_architectures:
  - amd64
  - i386
```

If you don't specify the `architecture` key, then the role will create
matching build chroots for all the architectures listed in
`sbuild_architectures` (which defaults to the host architecture only)
excluding those that are not allowed (when `allowed_architectures` is
set). In the above example, we will only have an amd64 chroot for
bullseye, but amd64 and i386 chroots for buster, and only an i386 chroot
for sid.

The `extra_aliases` can embed the `{arch}` string to generate
architecture-specific aliases. 

If you want to override the mirror used, you have to modify the data
returned by the `distro_info` role with variables like those:
```
distro_info_debian_default_mirror: "http://ftp.fr.debian.org/debian"
distro_info_ubuntu_default_mirror: "http://archive.fr.ubuntu.com/ubuntu"
distro_info_kali_default_mirror: "http://kali.download/kali"
```

The remaining parameters that you can tweak are the following:
```
# Change to /srv/buildd to use a directory with more space in case /var is small
sbuild_base_dir: /var/lib/sbuild
# Enable if you want to create a tmpfs to store the overlayfs directories
sbuild_enable_tmpfs: no

# Directory where we create the build chroots
sbuild_chroot_dir: /srv/chroots
# Default template
sbuild_schroot_template: sbuild-schroot-template.j2

# Install supplementary packages in the build chroot
sbuild_extra_packages:
  - eatmydata
# Enable the "deb-src" lines in sources.list
sbuild_enable_source_apt_entries: yes
# Whether we create /usr/sbin/policy-rc.d in the build chroot
sbuild_create_policy_rc_d: yes
# Whether to create a cron job to upgrade build chroots each day
sbuild_enable_automatic_update: yes
```

After the role has run, you can use the `sbuild_managed_chroots` variable
to perform further customizations to the build. It's a list of hashes with
two keys describing all the chroots that have been configured: `name` is
the chroot name as it's registered in schroot and `path` is the top-level
directory of the build chroot.

## Example Playbook

```
- hosts: buildd
  roles:
    - role: freexian.sbuild
  vars:
    sbuild_distributions:
      - kali-rolling
      - kali-dev
  tasks:
    - debug:
        msg: 'Yay, chroot {{ item.name }} is available in {{ item.path }}!'
      loop: '{{ sbuild_managed_chroots }}'
```

## License

MIT

## Author Information

Raphaël Hertzog <raphael@freexian.com>
